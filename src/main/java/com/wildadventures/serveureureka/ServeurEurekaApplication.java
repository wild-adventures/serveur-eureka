package com.wildadventures.serveureureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServeurEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServeurEurekaApplication.class, args);
	}
}
